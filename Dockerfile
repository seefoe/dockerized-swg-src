FROM i386/ubuntu:14.04
MAINTAINER seefo

# install build dependencies
run apt-get update
run apt-get install -y software-properties-common build-essential zlib1g-dev libpcre3-dev cmake libboost-dev libxml2-dev libncurses5-dev flex bison git-core alien libaio1 python-ply bc curl libcurl4-gnutls-dev

# add redists
add ./utils/redist/ /redist/

# install redists
run alien -i /redist/oracle-instantclient12.1-basic-12.1.0.2.0-1.i386.rpm
run alien -i /redist/oracle-instantclient12.1-devel-12.1.0.2.0-1.i386.rpm
run alien -i /redist/oracle-instantclient12.1-sqlplus-12.1.0.2.0-1.i386.rpm

run apt-get install -y wget

# install java
run cd /redist/ && \
	wget --no-check-certificate https://bitbucket.org/swgnoobs/dontask/downloads/jdk-8u73-linux-i586.tar.gz && \
	tar -xvzf jdk-8u73-linux-i586.tar.gz && \
	sudo mv jdk1.8.0_73/ /opt && \
	sudo ln -s /opt/jdk1.8.0_73 /usr/java

# build and install curl
add src/external/3rd/library/curl/curl-7.45.0 /redist/curl/
run cd /redist/curl && ./configure && make && make install

# env variables
ENV JAVA_HOME /usr/java
ENV JAVA_PATH /usr/java
ENV ORACLE_HOME /usr/lib/oracle/12.1/client
run export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/usr/lib/oracle/12.1/client/lib:/usr/include/oracle/12.1/client
run ldconfig

# build src
ENTRYPOINT cd /swg/ && ./build.sh
