rm -rf build/*

export _JAVA_HOME=/usr/java
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/usr/java/jre/lib/i386:/usr/java/jre/lib/i386/xawt
export PATH=$PATH:/usr/java/bin

ls /usr/java/jre/lib/i386/

cd build
cmake .. -DCMAKE_BUILD_TYPE=Release
make -j$(nproc)
